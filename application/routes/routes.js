module.exports = function(app) {
    var express = require('express')
    var router = express.Router();
    var controllers = app.get('controllers');

    router.route('/').get(function(request, response) {
        response.send('Aplicação levantada');
    });

    //Definindo cada rota
    router.route('/fuel/costs').get(controllers.FuelController.getCosts);
    router.route('/states').get(controllers.StateController.getStates);
    router.route('/city/:id_cidade').get(controllers.CityController.getCity);

    app.use('/', router);
};