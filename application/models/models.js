module.exports = function(app) {
    var State = require('./State');
    var City = require('./City');
    var Fuel = require('./Fuel');
    var ConsumerPrice = require('./ConsumerPrice');
    var DistributionPrice = require('./DistributionPrice');
    var StationPrice = require('./StationPrice');
    var Station = require('./Station');

    return {
        State: State,
        City: City,
        Fuel: Fuel,
        ConsumerPrice: ConsumerPrice,
        Station: Station,
        DistributionPrice: DistributionPrice,
        StationPrice: StationPrice
    }
};