// instancia do mongoose e Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var citySchema = new Schema({
    name: String,
    statistics: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Fuel'
    }],
    stations:[{
    	type: mongoose.Schema.Types.ObjectId,
    	ref: 'Station'
    }]
});

citySchema.index({ name: 1, type: -1 });

//Definindo o model
module.exports = mongoose.model('City', citySchema);
