// instancia do mongoose e Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Definindo o model
module.exports = mongoose.model('DistributionPrice', new Schema({
    averagePrice: String,
    standardDeviation: String,
    minPrice: String,
    maxPrice: String
}));
