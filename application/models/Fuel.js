// instancia do mongoose e Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var fuelSchema = new Schema({
    type: String,
    consumerPrice: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ConsumerPrice'
    }],
    distributionPrice: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ConsumerPrice'
    }]
});

fuelSchema.index({ type: 1, type: -1 });

//Definindo o model
module.exports = mongoose.model('Fuel', fuelSchema);