// instancia do mongoose e Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var stateSchema = new Schema({
    name: String,
    cities: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'City'
    }]
});

stateSchema.index({ name: 1, type: -1 });

//Definindo o model
module.exports = mongoose.model('State', stateSchema);