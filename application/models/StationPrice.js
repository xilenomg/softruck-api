// instancia do mongoose e Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Definindo o model
module.exports = mongoose.model('StationPrice', new Schema({
    type: String,
    sellPrice: String,
    buyPrice: String,
    saleMode: String,
    provider: String,
    date: String
}));
