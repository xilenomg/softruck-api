// instancia do mongoose e Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var stationSchema = new Schema({
    name: String,
    address: String,
    area: String,
    flag: String,
    prices: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'StationPrice'
    }]
});

stationSchema.index({ name: 1, type: -1 });
//Definindo o model
module.exports = mongoose.model('Station', stationSchema);