module.exports = function(app) {
    var getCity = function(request, response, next) {
        var id_cidade = request.params.id_cidade;
        var models = app.get('models');
        var options = [
            'statistics', 'stations', 'statistics.consumerPrice', 'statistics.distributionPrice', 'stations.prices'
        ];
        models.City.findOne({
            _id: id_cidade
        }).populate(options).exec(function(err, cidade) {


            if (err) {
                console.log(err);
                response.json([])
            } else {
                var options = [{
                    path: 'stations.prices',
                    model: 'StationPrice'
                }, {
                    path: 'statistics.consumerPrice',
                    model: 'ConsumerPrice'
                }, {
                    path: 'statistics.distributionPrice',
                    model: 'DistributionPrice'
                }];
                models.City.populate(cidade, options, function(err, cidade) {


                    if (err) {
                        console.log(err);
                        response.json([])
                    } else {
                        response.json(cidade);
                    }
                });
            }
        });
    };

    return {
        getCity: getCity
    }
};