module.exports = function(app) {
    var FuelController = require('./FuelController')(app);
    var StateController = require('./StateController')(app);
    var CityController = require('./CityController')(app);
    return {
        FuelController: FuelController,
        StateController: StateController,
        CityController: CityController
    }
};