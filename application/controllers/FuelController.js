var request = require('request');
var cheerio = require('cheerio');

module.exports = function(app) {

    var getEstados = function() {
        return [{
            codigoEstado: 'AC*ACRE',
            estado: 'Acre'
        }, {
            codigoEstado: 'AL*ALAGOAS',
            estado: 'Alagoas'
        }, {
            codigoEstado: 'AP*AMAPA',
            estado: 'Amapa'
        }, {
            codigoEstado: 'AM*AMAZONAS',
            estado: 'Amazonas'
        }, {
            codigoEstado: 'BA*BAHIA',
            estado: 'Bahia'
        }, {
            codigoEstado: 'CE*CEARA',
            estado: 'Ceara'
        }, {
            codigoEstado: 'DF*DISTRITO@FEDERAL',
            estado: 'Distrito Federal'
        }, {
            codigoEstado: 'ES*ESPIRITO@SANTO',
            estado: 'Espirito Santo'
        }, {
            codigoEstado: 'GO*GOIAS',
            estado: 'Goias'
        }, {
            codigoEstado: 'MA*MARANHAO',
            estado: 'Maranhao'
        }, {
            codigoEstado: 'MT*MATO@GROSSO',
            estado: 'Mato Grosso'
        }, {
            codigoEstado: 'MS*MATO@GROSSO@DO@SUL',
            estado: 'Mato Grosso do Sul'
        }, {
            codigoEstado: 'MG*MINAS@GERAIS',
            estado: 'Minas Gerais'
        }, {
            codigoEstado: 'PA*PARA',
            estado: 'Para'
        }, {
            codigoEstado: 'PB*PARAIBA',
            estado: 'Paraiba'
        }, {
            codigoEstado: 'PR*PARANA',
            estado: 'Parana'
        }, {
            codigoEstado: 'PE*PERNAMBUCO',
            estado: 'Pernambuco'
        }, {
            codigoEstado: 'PI*PIAUI',
            estado: 'Piaui'
        }, {
            codigoEstado: 'RJ*RIO@DE@JANEIRO',
            estado: 'Rio de Janeiro'
        }, {
            codigoEstado: 'RN*RIO@GRANDE@DO@NORTE',
            estado: 'Rio Grande do Norte'
        }, {
            codigoEstado: 'RS*RIO@GRANDE@DO@SUL',
            estado: 'Rio Grande do Sul'
        }, {
            codigoEstado: 'RO*RONDONIA',
            estado: 'Rondonia'
        }, {
            codigoEstado: 'RR*RORAIMA',
            estado: 'Roraima'
        }, {
            codigoEstado: 'SC*SANTA@CATARINA',
            estado: 'Santa Catarina'
        }, {
            codigoEstado: 'SP*SAO@PAULO',
            estado: 'Sao Paulo'
        }, {
            codigoEstado: 'SE*SERGIPE',
            estado: 'Sergipe'
        }, {
            codigoEstado: 'TO*TOCANTINS',
            estado: 'Tocantins'
        }];
    };

    var getCombustiveis = function() {
        return [{
            idCombustivel: 487,
            codigoCombustivel: '487*Gasolina',
            combustivel: 'Gasolina'
        }, {
            idCombustivel: 643,
            codigoCombustivel: '643*Etanol',
            combustivel: 'Etanol'
        }, {
            idCombustivel: 476,
            codigoCombustivel: '476*GNV',
            combustivel: 'GNV'
        }, {
            idCombustivel: 532,
            codigoCombustivel: '532*Diesel',
            combustivel: 'Diesel'
        }, {
            idCombustivel: 812,
            codigoCombustivel: '812*Diesel@S10',
            combustivel: 'Diesel S10'
        }, {
            idCombustivel: 462,
            codigoCombustivel: '462*GLP',
            combustivel: 'GLP'
        }];
    };

    var selSemana = '849*De 20/09/2015 a 26/09/2015';
    var desc_semana = 'de 20/09/2015 a 26/09/2015';
    var cod_Semana = '850';
    var tipo = '1';
    var Cod_Combustivel = 'undefined';
    var url = 'http://www.anp.gov.br/preco/prc/Resumo_Por_Estado_Municipio.asp';

    var criaOuLocalizaEstado = function(estado) {
        var models = app.get('models');
        var q = require('q');
        var deferred = q.defer();

        models.State.findOne({
            name: estado
        }, function(err, estadoDB) {
            if (err) {
                deferred.reject(err);
            }
            if (!estadoDB) {
                var newState = new models.State({
                    name: estado
                });
                newState.save(function(err, newStateDB) {
                    // console.log("SALVO: ", newStateDB.name);
                    if (err) {
                        deferred.reject(err);
                    } else {
                        deferred.resolve(newStateDB);
                    }
                });
            } else {
                deferred.resolve(estadoDB);
            }
        });
        return deferred.promise;
    };

    var criaOuLocalizaCidade = function(estado, cidade) {
        var models = app.get('models');
        var q = require('q');
        var deferred = q.defer();
        models.City.findOne({
            _id: {
                $in: estado.cities
            },
            name: cidade
        }, function(err, cidadeDB) {
            if (err) {
                deferred.reject(err);
            } else {
                if (cidadeDB) {
                    deferred.resolve(cidadeDB);
                } else {
                    var newCidade = new models.City({
                        name: cidade
                    });
                    newCidade.save(function(err) {
                        if (err) {
                            deferred.reject(err);
                        } else {
                            // console.log("PUSH: ", newCidade);
                            estado.cities.push(newCidade._id);
                            estado.save(function(err) {
                                if (err) {
                                    deferred.reject(err);
                                } else {
                                    deferred.resolve(newCidade);
                                }
                            });
                        }
                    });

                }
            }
        });

        return deferred.promise;
    };

    var criaOuLocalizaCombustivel = function(cidade, combustivel) {
        var models = app.get('models');
        var q = require('q');
        var deferred = q.defer();

        models.Fuel.findOne({
            _id: {
                $in: cidade.statistics
            },
            type: combustivel
        }, function(err, combustivelDB) {
            if (err) {
                deferred.reject(err);
            } else {
                if (combustivelDB) {
                    deferred.resolve(combustivelDB);
                } else {
                    var newCombustivel = new models.Fuel({
                        type: combustivel
                    });
                    newCombustivel.save(function(err) {
                        if (err) {
                            deferred.reject(err);
                        }
                        cidade.statistics.push(newCombustivel._id);
                        cidade.save(function(err) {
                            if (err) {
                                deferred.reject(err);
                            } else {
                                deferred.resolve(newCombustivel);
                            }
                        });
                    });
                }
            }
        });

        return deferred.promise;
    };

    var requestCombustivelEstado = function(estado, combustivel) {
        var models = app.get('models');
        var q = require('q');
        var deferred = q.defer();

        request.post(url, {
            form: {
                selEstado: estado.codigoEstado,
                selCombustivel: combustivel.codigoCombustivel,
                selSemana: selSemana,
                desc_semana: desc_semana,
                cod_Semana: cod_Semana,
                tipo: tipo,
                Cod_Combustivel: Cod_Combustivel
            }
        }, function(err, resp, body) {
            if (!err && resp.statusCode == 200) {
                var $ = cheerio.load(body);
                var linhas = [];
                criaOuLocalizaEstado(estado.estado).then(function(estadoDB) {
                    $("tr").each(function(i, element) {
                        var row = $(element).find('td');
                        if (row.length > 5) {
                            criaOuLocalizaCidade(estadoDB, $(row[0]).text()).then(function(cidadeDB) {

                                //extrai o codigo da cidade da URL
                                var codigoCidade = $(row[0]).find("a").attr("href").replace('javascript:Direciona(\'', '').replace('\');', '');

                                process.nextTick(function() {
                                    criaOuLocalizaCombustivel(cidadeDB, combustivel.combustivel).then(function(combustivelDB) {
                                        var consumerPrice = new models.ConsumerPrice({
                                            averagePrice: ($(row[2]).text()),
                                            standardDeviation: ($(row[3]).text()),
                                            minPrice: ($(row[4]).text()),
                                            maxPrice: ($(row[5]).text()),
                                            averageMargin: ($(row[6]).text())
                                        });

                                        var distributionPrice = new models.DistributionPrice({
                                            averagePrice: ($(row[7]).text()),
                                            standardDeviation: ($(row[8]).text()),
                                            minPrice: ($(row[9]).text()),
                                            maxPrice: ($(row[10]).text())
                                        });
                                        distributionPrice.save(function(err) {
                                            combustivelDB.distributionPrice.push(distributionPrice._id);
                                            consumerPrice.save(function(err) {
                                                combustivelDB.consumerPrice.push(consumerPrice._id);
                                                combustivelDB.save(function(err) {
                                                    if (err) {
                                                        deferred.reject(err);
                                                    } else {
                                                        process.nextTick(function() {
                                                            requestStationsCidade(codigoCidade, combustivel, cidadeDB).then(function(resolve) {
                                                                deferred.resolve(combustivelDB);
                                                            }, function(err) {
                                                                console.log("RESOLVIDO COM ERRO", err);
                                                                deferred.reject(err);
                                                            });
                                                        });
                                                    }
                                                });
                                            });
                                        });


                                    }, function(err) {
                                        console.log("err: ", err);
                                        deferred.reject(err);
                                    });
                                });



                            }, function(err) {
                                console.log("err: ", err);
                                deferred.reject(err);
                            });
                        } else {
                            deferred.reject("linha < 5");
                        }

                    });
                }, function(err) {
                    console.log("err: ", err);
                    deferred.reject(err);
                });
            } else {
                deferred.reject(err);
            }
        });

        return deferred.promise;
    };

    var criaOuLocalizaEstacoes = function(station, cidadeDB) {
        var models = app.get('models');
        var q = require('q');
        var deferred = q.defer();
        var stations = cidadeDB.stations;
        models.Station.findOne({
            name: station.name,
            _id: {
                $in: stations
            }
        }, function(err, stationDB) {
            if (err) {
                deferred.reject(err);
            } else {

                //station existe
                if (stationDB) {
                    deferred.resolve(stationDB);
                }
                //station nao existe
                else {
                    var newStation = new models.Station(station);

                    newStation.save(function(err) {
                        if (err) {
                            deferred.reject(err);
                        } else {
                            cidadeDB.stations.push(newStation);
                            cidadeDB.save(function() {
                                if (err) {
                                    deferred.reject(err);
                                } else {
                                    deferred.resolve(newStation);
                                }
                            })
                        }
                    });
                }
            }
        });
        return deferred.promise;
    };

    var requestStationsCidade = function(codigoCidade, combustivel, cidadeDB) {
        var models = app.get('models');
        var q = require('q');
        var deferred = q.defer();
        request.post('http://www.anp.gov.br/preco/prc/Resumo_Semanal_Posto.asp', {
            form: {
                cod_semana: cod_Semana,
                desc_semana: desc_semana,
                cod_combustivel: combustivel.idCombustivel,
                desc_combustivel: ' - Gasolina R$/l',
                selMunicipio: codigoCidade,
                tipo: tipo
            }
        }, function(err, resp, body) {
            if (!err && resp.statusCode == 200) {
                var $ = cheerio.load(body);
                $("tr").each(function(i, element) {
                    var row = $(element).find('td');
                    if (row.length === 9) {
                        var station = {
                            name: $(row[0]).text(),
                            address: $(row[1]).text(),
                            area: $(row[2]).text(),
                            flag: $(row[3]).text()
                        }
                        process.nextTick(function() {
                            criaOuLocalizaEstacoes(station, cidadeDB).then(function(stationDB) {

                                    var stationPrice = new models.StationPrice({
                                        type: combustivel.combustivel,
                                        sellPrice: ($(row[4]).text()),
                                        buyPrice: ($(row[5]).text()),
                                        saleMode: $(row[6]).text(),
                                        provider: $(row[7]).text(),
                                        date: $(row[8]).text()
                                    });

                                    stationPrice.save(function(err) {
                                        if (err) {
                                            deferred.reject(err);
                                        } else {
                                            stationDB.prices.push(stationPrice);
                                            stationDB.save(function(err) {
                                                if (err) {
                                                    deferred.reject(err);
                                                } else {
                                                    deferred.resolve(stationDB);
                                                }
                                            });
                                        }
                                    });
                                },
                                function(err) {
                                    console.log(error);
                                });
                        });
                    }
                });
            }
        });
        return deferred.promise;
    };

    var criaJSON = function(request, response) {
        var models = app.get('models');
        var startTime = (new Date()).getTime();
        models.State.find().populate('cities').exec(function(err, states) {
            var options = [{
                path: 'cities.statistics',
                model: 'Fuel'
            }, {
                path: 'cities.stations',
                model: 'Station'
            }];
            models.State.populate(states, options, function(err, states) {

                var options = [{
                    path: 'cities.statistics.consumerPrice',
                    model: 'ConsumerPrice'
                }, {
                    path: 'cities.statistics.distributionPrice',
                    model: 'DistributionPrice'
                }, {
                    path: 'cities.stations',
                    model: 'Station'
                }, {
                    path: 'cities.stations.prices',
                    model: 'StationPrice'
                }];
                models.State.populate(states, options, function(err, states) {
                    var endTime = (new Date()).getTime();
                    console.log('Time taken:', endTime - startTime);
                    response.json(states);
                });
            });

        });
    };

    var atualizarMongoDB = function(request, response) {

        var estados = getEstados();

        var combustives = getCombustiveis();

        var total = 0;
        var contador = 0;
        combustives.forEach(function(combustivel) {
            estados.forEach(function(estado) {
                total++;
                process.nextTick(function() {
                    requestCombustivelEstado(estado, combustivel).then(function(combustivel) {
                        // console.log("SUCESSO: ", contador, total);
                        contador++;
                        if (contador === total - 1) {
                            criaJSON(request, response);
                        }
                    }, function(err) {
                        // console.log("erro na request: ", err);
                        contador++;
                        // console.log("ERRO: ", contador, total);
                        if (contador === total - 1) {
                            criaJSON(request, response);
                        }
                    });
                });

            });
        });
    };

    //metodos publicos
    var getCosts = function(request, response) {
        var models = app.get('models');
        models.StationPrice.findOne({}, function(err, consumerPrice) {
            if (consumerPrice) {
                console.log("CRIA JSON");
                criaJSON(request, response);
            } else {
                console.log("atualizarMongoDB");
                atualizarMongoDB(request, response);
            }
        });
    };

    return {
        getCosts: getCosts
    }
};