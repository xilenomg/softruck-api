module.exports = function(app) {
    var getStates = function(request, response, next) {
        var models = app.get('models');
        models.State.find({}, function(err, states) {
            if (err) {
                console.log(err);
                response.json([])
            } else {
                response.json(states);
            }
        })
    };

    return {
        getStates: getStates
    }
};