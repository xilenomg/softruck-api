var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    configuration = require('./configuration');

/************
Configuração
*************/

//define porta da aplicacao
var porta = process.env.PORT || 3000;

// Add headers
app.use(function(request, response, next) {
    response.setHeader('Access-Control-Allow-Origin', 'http://localhost:3001');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    response.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//conexão com o mongoDB
mongoose.connect(configuration.mongo.connectionString);

//middleware para acessar as informações que forem recebidas por POST facilmente
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

var controllers = require('./application/controllers/controllers')(app);
app.set('controllers', controllers);
var models = require('./application/models/models')(app);
app.set('models', models);
var routes = require('./application/routes/routes')(app);
app.set('routes', routes);




//levanta o servidor na porta: porta
app.listen(porta, function() {
    console.log("Aplicação disponível na porta: " + porta);
});


//Exceções que não foram tratadas e possam derrubar o servidor
// process.on('uncaughtException', function(err) {
//     console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
//     console.log('Caught exception:');
//     console.log(err);
//     console.log('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
// });